# Module Example_Instagram

Add Instagram photo widget with admin panel.

## Installation of the base module
1. Create directory app/code/Example/Instagram
2. Copy repository there (without `examples` folder)
3. Run 
    ```
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento indexer:reindex
php bin/magento cache:clean
```

## How To

Use widget block "Example\Instagram\Block\Widget"

Its template placed in "view\frontend\templates\widget.phtml"

Admin panel placed in the standard admin configuration - Stores > Configuration > Example > Instagram