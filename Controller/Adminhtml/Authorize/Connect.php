<?php

namespace Example\Instagram\Controller\Adminhtml\Authorize;

use \Magento\Backend\App\Action;
use \Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\ResultFactory;
use \Example\Instagram\Helper\Config as ModuleConfig;
use \Example\Instagram\Instagram\Api\Adapter\AdapterInterface as InstagramApiInterface;

/**
 * Class Connect
 * Instagram connect action.
 *
 * @package Example\Instagram\Controller\Adminhtml\Authorize
 * @author Eugene Polischuk <eugene.polischuk@example.com>
 * @copyright Copyright (c) 2017 Example (https://www.example.com/)
 */
class Connect extends Action
{
    /**
     * @var ModuleConfig
     */
    protected $moduleConfig;

    /**
     * @var InstagramApiInterface
     */
    protected $instagramApi;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param ModuleConfig $moduleConfig
     * @param InstagramApiInterface $instagramApi
     * @author Eugene Polischuk <eugene.polischuk@example.com>
     */
    public function __construct(
        Context $context,
        ModuleConfig $moduleConfig,
        InstagramApiInterface $instagramApi
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->instagramApi = $instagramApi;

        parent::__construct($context);
    }

    /**
     * @return mixed
     * @author Eugene Polischuk <eugene.polischuk@example.com>
     */
    public function execute()
    {
        $this->moduleConfig->setStoreId(
            $this->getRequest()->getParam('store')
        );

        $this->moduleConfig->deleteError();
        $this->moduleConfig->deleteToken();

        $this->moduleConfig->setClientIdValue(
            $this->getRequest()->getParam('client_id')
        );
        $this->moduleConfig->setClientSecretValue(
            $this->getRequest()->getParam('client_secret')
        );
        $this->moduleConfig->setIsEnabledValue(
            $this->getRequest()->getParam('is_enabled')
        );
        $this->moduleConfig->refreshConfig();

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)
            ->setUrl($this->instagramApi->getLoginUrl());
    }
}
